FROM node:16.20.2-alpine3.18 as builder
WORKDIR /app
COPY package*.json ./
RUN npm install --include=dev
COPY . .
RUN npm run build

FROM node:16.20.2-alpine3.18
WORKDIR /app
COPY --from=builder /app ./
ENTRYPOINT npm start
EXPOSE 8080

